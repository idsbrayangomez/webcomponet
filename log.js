//-----------------cracion de los web componet
class brrarBusqueda extends HTMLElement{
    constructor(){
        const tpl = document.querySelector('#buscar-datos');
        const tplInst = tpl.content.cloneNode(true);
        super();
        this.attachShadow({mode: 'open'});
        this.shadowRoot.appendChild(tplInst);
    }
}
customElements.define('barra-de-busqueda', brrarBusqueda);

class Tabla extends HTMLElement{
    constructor(){
        const tpl = document.querySelector('#tabla-datos');
        const tplInst = tpl.content.cloneNode(true);
        super();
        this.attachShadow({mode: 'open'});
        this.shadowRoot.appendChild(tplInst);
    }
}
customElements.define('mi-tabla', Tabla);
//---------------------------------------------------------------

//cracion del boton buscar con el evento click
//para llamar a la funcion buscar
class botonBuscar extends HTMLButtonElement{
    constructor(){
        super();
        this.addEventListener('click', function(e){
            buscar();
        });
    }
}
customElements.define('boton-buscar', botonBuscar, {extends:'button'});

//Creacion del boton borrar con evento click
//para llamar a la funcion borrar
class botonBorar extends HTMLButtonElement{
    constructor(){
        super();
        this.addEventListener('click', function(e){
            borartabla();
        });
    }
}
customElements.define('boton-borrar',botonBorar, {extends:'button'});




function buscar(){
    
    var elem = document.getElementById('barra-navegacion').shadowRoot;
    const valor_de_busqueda = elem.childNodes[1].value;
    
    //se hace la peticion con fetch
    var url = "https://jsonplaceholder.typicode.com/posts";
    fetch(url)
    .then( response =>{
        return response.json();
    })
    .then(data =>{
        mostrardatos(valor_de_busqueda, data);
    })
    .catch( err =>{
        console.log(err);
    });
}



 function mostrardatos(valor_buscado, objeto){
     console.log(objeto);
    for(var i = 0; i <= objeto.length; i++){

        id = objeto[i]['id'];
        //compara el id con el que esta buscando 
        if(valor_buscado == id){
            body = objeto[i]['body'];
            userID = objeto[i]['userId'];
            titulo = objeto[i]['title'];
            //se optine la tabla y se guardar en una variable con su shadowRoot
            var tabla = document.getElementById('mi-tabla').shadowRoot;
            
            //se guardan los nodos hijos en su variable 
            var columnaId = tabla.childNodes[1].childNodes[1].childNodes[2].childNodes[0];
            //se le pasa el valor que va apintar
            columnaId.textContent = id;
            var columnaId = tabla.childNodes[1].childNodes[1].childNodes[2].childNodes[1];
            columnaId.textContent = titulo;
            var columnaId = tabla.childNodes[1].childNodes[1].childNodes[2].childNodes[2];
            columnaId.textContent = body;
        } 
    }
}

//fincion para poner en blanco la tabla 
function borartabla(){
    var tabla = document.getElementById('mi-tabla').shadowRoot;
    var columnaId = tabla.childNodes[1].childNodes[1].childNodes[2].childNodes[0];
    columnaId.textContent = "";
    var columnaId = tabla.childNodes[1].childNodes[1].childNodes[2].childNodes[1];
    columnaId.textContent = "";
    var columnaId = tabla.childNodes[1].childNodes[1].childNodes[2].childNodes[2];
    columnaId.textContent = "";
}